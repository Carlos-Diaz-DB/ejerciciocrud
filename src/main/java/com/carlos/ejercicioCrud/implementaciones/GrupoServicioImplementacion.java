package com.carlos.ejercicioCrud.implementaciones;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carlos.ejercicioCrud.entidades.Grupo;
import com.carlos.ejercicioCrud.repositorio.GrupoRepositorio;
import com.carlos.ejercicioCrud.servicios.GrupoServicio;

@Service("GrupoServicio")
public class GrupoServicioImplementacion implements GrupoServicio{

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
    public GrupoRepositorio grupoRepositorio;
	
	@Override
    public boolean agregarGrupo(Grupo grupo)
    {
        try
        {
          if (grupo != null)
            {
        	  grupoRepositorio.save(grupo);
                log.info("El grupo fue añadido: "+grupo.toString());
                return true;
            }
            log.warn("Grupo sin datos");
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR:  "+ex.getMessage());
            return false;
        }

    }
	
	 @Override
	    public boolean actualizarGrupo(Grupo grupo)
	    {
	        try
	        {
	            if (grupo != null)
	            {
	            	grupoRepositorio.save(grupo);
	                log.info("Grupo actualizado: "+grupo.toString());
	                return true;
	            }
	            log.warn("Grupo sin datos");
	            return false;
	        }
	        catch (Exception ex)
	        {
	            log.error("ERROR: "+ex.getMessage());
	            return false;
	        }
	    }
	 
	 @Override
	    public boolean borrarGrupo(Grupo grupo)
	    {
			try
			{
				if (grupo != null)
				{
					grupoRepositorio.save(grupo);
					log.info("Grupo actualizado: "+grupo.toString());
					return true;
				}
				log.warn("Grupo sin datos");
				return false;
			}
			catch (Exception ex)
			{
				log.error("ERROR: "+ex.getMessage());
				return false;
			}
	    }
	 
	 @Override
	    public Grupo obtenerGrupo(int grupoId)
	    {
	        try {
	            if (grupoId > 0)
	            {
	                log.info("Se consulto: ID de grupo: "+grupoId);
	                return grupoRepositorio.findById(grupoId).get();
	            }
	            return null;
	        }
	        catch (Exception ex)
	        {
	            log.error("ERROR: "+ex.getMessage());
	            return null;
	        }
	    }
	 
	 @Override
	    public List<Grupo> listarGrupos()
	    {

	        try
	        {
	        	
	        	
	            log.info("Lista de grupos\n");
	            return (List<Grupo>) grupoRepositorio.findAll();
	        }
	        catch (Exception ex)
	        {
	            log.error("ERROR: "+ex.getMessage());
	        }
	        return null;
	    }

}
