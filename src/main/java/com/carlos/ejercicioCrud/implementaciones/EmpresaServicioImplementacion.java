package com.carlos.ejercicioCrud.implementaciones;

import java.util.List;

import com.carlos.ejercicioCrud.entidades.Grupo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carlos.ejercicioCrud.entidades.Empresa;
import com.carlos.ejercicioCrud.repositorio.EmpresaRepositorio;
import com.carlos.ejercicioCrud.servicios.EmpresaServicio;

@Service("EmpresaServicio")
public class EmpresaServicioImplementacion implements EmpresaServicio{

	private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public EmpresaRepositorio empresaRepositorio;
    
    @Override
    public boolean agregarEmpresa(Empresa empresa)
    {
        try
        {
          if (empresa != null)
            {
        	  empresaRepositorio.save(empresa);
                log.info("Se agrego datos de empresa corrctamente: "+empresa.toString());
                return true;
            }
            log.warn("Datos invalidos agregar empresa");
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR:  "+ex.getMessage());
            return false;
        }

    }

    @Override
    public boolean actualizarEmpresa(Empresa empresa)
    {
        try
        {
            if (empresa != null)
            {
            	empresaRepositorio.save(empresa);
                log.info("Se actualizo el task: "+empresa.toString());
                return true;
            }
            log.warn("datos invalidos actualizar empresa");
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean borrarEmpresa(Empresa empresa)
    {
        try
        {
            if (empresa != null)
            {
            	empresaRepositorio.delete(empresa);
                log.info("Se borro la empresa correctamente");
                return true;
            }
            log.warn("datos de empresa vacio en borrarempresa");
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }

    @Override
    public Empresa obtenerEmpresa(int idEmpresa)
    {
        try {
            if (idEmpresa > 0)
            {
                log.info("Se consulto: ID:Empresa "+idEmpresa);
                return empresaRepositorio.findById(idEmpresa).get();
            }
            return null;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return null;
        }
    }
    
    @Override
    public List<Empresa> listarEmpresas(int idGrupo)
    {

    	try {
			return (List<Empresa>) empresaRepositorio.findByGrupoId(idGrupo);
		} catch (Exception ex) {
		}
        return null;
    }
  
}
