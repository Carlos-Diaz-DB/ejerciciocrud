package com.carlos.ejercicioCrud.implementaciones;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carlos.ejercicioCrud.entidades.Sucursal;
import com.carlos.ejercicioCrud.repositorio.SucursalRepositorio;
import com.carlos.ejercicioCrud.servicios.SucursalServicio;

@Service("SucursalServicio")
public class SucursalServicioImplementacion implements SucursalServicio{

	
	private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public SucursalRepositorio sucursalRepositorio;
    
    @Override
    public boolean agregarSucursal(Sucursal sucursal)
    {
        try
        {
          if (sucursal != null)
            {
        	  sucursalRepositorio.save(sucursal);
                log.info("Se agrego datos de sucursal corrctamente: "+sucursal.toString());
                return true;
            }
            log.warn("Datos invalidos agregar sucursal");
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR:  "+ex.getMessage());
            return false;
        }

    }

    @Override
    public boolean actualizarSucursal(Sucursal sucursal)
    {
        try
        {
            if (sucursal != null)
            {
            	sucursalRepositorio.save(sucursal);
                log.info("Se actualizo sucursal: "+sucursal.toString());
                return true;
            }
            log.warn("datos invalidos actualizar sucursal");
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean borrarSucursal(Sucursal sucursal)
    {
        try
        {
            if (sucursal != null)
            {
            	sucursalRepositorio.delete(sucursal);
                log.info("Se borro la sucursal correctamente");
                return true;
            }
            log.warn("datos de sucursal vacio en borrarsucursal");
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }
    
    @Override
    public Sucursal obtenerSucursal(int idSucursal)
    {
        try {
            if (idSucursal > 0)
            {
                log.info("Se consulto: ID:Sucursal "+idSucursal);
                return sucursalRepositorio.findById(idSucursal).get();
            }
            return null;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return null;
        }
    }

    
    @Override
    public List<Sucursal> listarSucursales(int idEmpresa)
    {

    	try {
			return (List<Sucursal>) sucursalRepositorio.findByEmpresaId(idEmpresa);
		} catch (Exception ex) {
		}
        return null;
    }
}
