package com.carlos.ejercicioCrud.Controlador;

import java.util.List;

import com.carlos.ejercicioCrud.entidades.Grupo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carlos.ejercicioCrud.entidades.Empresa;
import com.carlos.ejercicioCrud.servicios.EmpresaServicio;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(path = "/")
public class EmpresaControlador {

	 private Logger log = LoggerFactory.getLogger(this.getClass());
	    private ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,false);

	    @Autowired
	    @Qualifier("EmpresaServicio")
	    private EmpresaServicio empresaServicio;
	    
	    @RequestMapping(path = "empresa", method = RequestMethod.POST)
	    public @ResponseBody boolean agregarEmpresa(@RequestBody String archivoJSON)
	    {
	        try
	        {
	            Empresa empresa = new Empresa();

	            empresa = mapper.readValue(archivoJSON,Empresa.class);
	            log.info("Se recibio del formulario: \n"+empresa.toString());

	            if (empresaServicio.agregarEmpresa(empresa))
	            {
	                log.info("Se cargo el archivo correctamente añadir empresa en controlador.");
	                return true;
	            }
	            return false;
	        }
	        catch (Exception ex)
	        {
	            log.error("ERROR: "+ex.getMessage());
	            return false;
	        }
	    }
	    
	    @RequestMapping(path = "empresa/{idGrupo}", method = RequestMethod.GET)
	    public @ResponseBody List<Empresa> listarEmpresas(@PathVariable("idGrupo") int idGrupo)
	    {
	        try
	        {
	            
	            return empresaServicio.listarEmpresas(idGrupo);
	        }
	        catch (Exception ex)
	        {
	            log.error("ERROR: "+ex.getMessage());
	            return null;
	        }
	    }
	    
	    
	    @RequestMapping(path="empresa",method = RequestMethod.PUT)
	    public @ResponseBody boolean actualizarEmpresa(@RequestBody String archivoJSON){
	        try{
	            Empresa empresa = new Empresa();
	            empresa = mapper.readValue(archivoJSON, Empresa.class);
	            log.info("Se recibio del formulario: \n"+empresa.toString());
	            if (empresaServicio.actualizarEmpresa(empresa)){
	                log.info("Se actualizo empresa correctamente");
	                return true;
	            }
	            return false;
	        }catch (Exception ex){
	            log.error("ERROR: "+ex.getMessage());
	            return false;
	        }
	    }
	    
	   
	    
	    

	    

	    @RequestMapping(path = "empresa", method = RequestMethod.DELETE)
	    public @ResponseBody boolean borrarEmpresa(@RequestBody String archivoJSON)
	    {
	        try
	        {
	           if(archivoJSON != null)
	           {
	               Empresa empresa = mapper.readValue(archivoJSON, Empresa.class);

	               return empresaServicio.borrarEmpresa(empresa);
	           }
	           log.warn(("El archivo llega vacio borrarEmresa controlador"));
	           return false;
	        }
	        catch(Exception ex)
	        {
	            log.error(ex.getMessage());
	            return false;
	        }
	    }
}
