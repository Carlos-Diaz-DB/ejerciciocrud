package com.carlos.ejercicioCrud.Controlador;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carlos.ejercicioCrud.entidades.Sucursal;
import com.carlos.ejercicioCrud.servicios.SucursalServicio;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(path = "/")
public class SucursalControlador {

	private Logger log = LoggerFactory.getLogger(this.getClass());
    private ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,false);

    @Autowired
    @Qualifier("SucursalServicio")
    private SucursalServicio sucursalServicio;
    
    @RequestMapping(path = "sucursal", method = RequestMethod.POST)
    public @ResponseBody boolean agregarSucursal(@RequestBody String archivoJSON)
    {
        try
        {
            Sucursal sucursal = new Sucursal();

            sucursal = mapper.readValue(archivoJSON,Sucursal.class);
            log.info("Se recibio del formulario: \n"+sucursal.toString());

            if (sucursalServicio.agregarSucursal(sucursal))
            {
                log.info("Se cargo el archivo correctamente añadir sucursal en controlador.");
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }
    
    @RequestMapping(path = "sucursal/{idEmpresa}", method = RequestMethod.GET)
    public @ResponseBody List<Sucursal> listarSucursales(@PathVariable("idEmpresa") int idSucursal)
    {
        try
        {
            
            return sucursalServicio.listarSucursales(idSucursal);
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return null;
        }
    }
    
    
    @RequestMapping(path="sucursal",method = RequestMethod.PUT)
    public @ResponseBody boolean actualizarSucursal(@RequestBody String archivoJSON){
        try{
            Sucursal sucursal = new Sucursal();
            sucursal = mapper.readValue(archivoJSON, Sucursal.class);
            log.info("Se recibio del formulario: \n"+sucursal.toString());
            if (sucursalServicio.actualizarSucursal(sucursal)){
                log.info("Se actualizo sucursal correctamente ctl");
                return true;
            }
            return false;
        }catch (Exception ex){
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }
    
   
    
    

    

    @RequestMapping(path = "sucursal", method = RequestMethod.DELETE)
    public @ResponseBody boolean borrarSucursal(@RequestBody String archivoJSON)
    {
        try
        {
           if(archivoJSON != null)
           {
        	   Sucursal sucursal = mapper.readValue(archivoJSON, Sucursal.class);

               return sucursalServicio.borrarSucursal(sucursal);
           }
           log.warn(("El archivo llega vacio borrarEmresa controlador"));
           return false;
        }
        catch(Exception ex)
        {
            log.error(ex.getMessage());
            return false;
        }
    }
}
