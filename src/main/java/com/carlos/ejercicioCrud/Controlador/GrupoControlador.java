package com.carlos.ejercicioCrud.Controlador;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carlos.ejercicioCrud.entidades.Empresa;
import com.carlos.ejercicioCrud.entidades.Grupo;
import com.carlos.ejercicioCrud.servicios.GrupoServicio;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(path = "/")
public class GrupoControlador {

	private Logger log = LoggerFactory.getLogger(this.getClass());
    private ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,false);

    @Autowired
    @Qualifier("GrupoServicio")
    private GrupoServicio grupoServicio;
    
    @RequestMapping(path = "grupo", method = RequestMethod.POST)
    public @ResponseBody boolean agregarGrupo(@RequestBody String archivoJSON)
    {
        try
        {
            Grupo grupo = new Grupo();

            grupo = mapper.readValue(archivoJSON,Grupo.class);
            log.info("Se recibio del formulario: \n"+grupo.toString());

            if (grupoServicio.agregarGrupo(grupo))
            {
                log.info("Se cargo el archivo correctamente añadir grupo en controlador.");
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }
    
    @RequestMapping(path = "grupo", method = RequestMethod.GET)
    public @ResponseBody List<Grupo> listarGrupos()
    {
        try
        {
            return grupoServicio.listarGrupos();
        }
        catch (Exception ex)
        {
            log.error("ERROR: "+ex.getMessage());
            return null;
        }
    }
    
    
    @RequestMapping(path="grupo",method = RequestMethod.PUT)
    public @ResponseBody boolean actualizarGrupo(@RequestBody String archivoJSON){
        try{
        	Grupo grupo = new Grupo();
        	grupo = mapper.readValue(archivoJSON, Grupo.class);
            log.info("Se recibio del formulario: \n"+grupo.toString());
            if (grupoServicio.actualizarGrupo(grupo)){
                log.info("Se actualizo grupo correctamente");
                return true;
            }
            return false;
        }catch (Exception ex){
            log.error("ERROR: "+ex.getMessage());
            return false;
        }
    }


}
