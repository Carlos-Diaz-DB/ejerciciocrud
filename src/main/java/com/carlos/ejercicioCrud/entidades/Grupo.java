package com.carlos.ejercicioCrud.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name="grupo")
public class Grupo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idGrupo", nullable = false)
	private int id;
	
	
	@NotNull
    @Size(min = 2, max = 120)
    @Column(length = 120, nullable = false)
	private String nombre;
	
	@NotNull
	@Column(columnDefinition = "TINYINT")
	private char status;

	public Grupo() {
		super();
	}

	public Grupo(int id, @NotNull @Size(min = 2, max = 120) String nombre, @NotNull char status) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.status = status;
	}

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public char getstatus() {
		return status;
	}

	public void setstatus(char status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Grupo [id=" + id + ", nombre=" + nombre + ", status=" + status + "]";
	}
	
	
	
}
