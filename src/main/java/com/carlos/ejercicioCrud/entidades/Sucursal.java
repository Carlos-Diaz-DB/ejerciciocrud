package com.carlos.ejercicioCrud.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="sucursal")
public class Sucursal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idSucursal", nullable = false)
	private int id;
	
	@NotNull
    @Size(min = 2, max = 120)
    @Column(length = 120, nullable = false)
	private String nombre;
	
	@NotNull
	@Column(columnDefinition = "TINYINT")
	private char status;
	
	@ManyToOne
	@JoinColumn(name="idEmpresa", nullable = false)
	private Grupo empresa;

	public Sucursal() {
		super();
	}

	public Sucursal(int id, @NotNull @Size(min = 2, max = 120) String nombre, @NotNull char status, Grupo empresa) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.status = status;
		this.empresa = empresa;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public Grupo getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Grupo empresa) {
		this.empresa = empresa;
	}

	@Override
	public String toString() {
		return "Sucursal [id=" + id + ", nombre=" + nombre + ", status=" + status + ", empresa=" + empresa + "]";
	}
	
	
}
