package com.carlos.ejercicioCrud.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name="empresa")
public class Empresa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idEmpresa", nullable = false)
	private int id;
	
	@NotNull
    @Size(min = 2, max = 120)
    @Column(length = 120, nullable = false)
	private String nombre;
	
	@NotNull
	@Column(columnDefinition = "TINYINT")
	private char status;
	
	@ManyToOne
	@JoinColumn(name="idGrupo", nullable = false)
	private Grupo grupo;

	public Empresa() {
		super();
	}

	public Empresa(int id, @NotNull @Size(min = 2, max = 120) String nombre, @NotNull char status,
			Grupo idGrupo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.status = status;
		this.grupo = idGrupo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public char getstatus() {
		return status;
	}

	public void setstatus(char status) {
		this.status = status;
	}

	public Grupo getIdGrupo() {
		return grupo;
	}

	public void setIdGrupo(Grupo idGrupo) {
		this.grupo = idGrupo;
	}

	@Override
	public String toString() {
		return "Empresa [id=" + id + ", nombre=" + nombre + ", status=" + status + ", idGrupo=" + grupo
				+ "]";
	}
	
	
}
