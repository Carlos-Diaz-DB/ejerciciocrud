package com.carlos.ejercicioCrud.servicios;

import java.util.List;

import com.carlos.ejercicioCrud.entidades.Sucursal;

public interface SucursalServicio {

	public boolean agregarSucursal(Sucursal sucursal);
    public boolean actualizarSucursal(Sucursal sucursal);
    public boolean borrarSucursal(Sucursal sucursal);
    public Sucursal obtenerSucursal(int sucursal);
    public List<Sucursal> listarSucursales(int idEmpresa);
}
