package com.carlos.ejercicioCrud.servicios;

import java.util.List;

import com.carlos.ejercicioCrud.entidades.Grupo;

public interface GrupoServicio {

	public boolean agregarGrupo(Grupo grupo);
    public boolean actualizarGrupo(Grupo grupo);
    public boolean borrarGrupo(Grupo grupo);
    public Grupo obtenerGrupo(int grupo);
    public List<Grupo> listarGrupos();
}
