package com.carlos.ejercicioCrud.servicios;

import java.util.List;

import com.carlos.ejercicioCrud.entidades.Empresa;
import com.carlos.ejercicioCrud.entidades.Grupo;

public interface EmpresaServicio {

	public boolean agregarEmpresa(Empresa empresa);
    public boolean actualizarEmpresa(Empresa empresa);
    public boolean borrarEmpresa(Empresa empresa);
    public Empresa obtenerEmpresa(int empresa);
    public List<Empresa> listarEmpresas(int idGrupo);
}
