package com.carlos.ejercicioCrud.repositorio;

import java.io.Serializable;
import java.util.List;

import com.carlos.ejercicioCrud.entidades.Grupo;
import org.springframework.data.repository.CrudRepository;
import com.carlos.ejercicioCrud.entidades.Empresa;

public interface EmpresaRepositorio extends CrudRepository<Empresa, Serializable> {
	List<Empresa> findByGrupoId(int idGrupo);
}

