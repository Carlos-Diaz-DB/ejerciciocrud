package com.carlos.ejercicioCrud.repositorio;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.carlos.ejercicioCrud.entidades.Sucursal;


public interface SucursalRepositorio extends CrudRepository<Sucursal, Serializable> {
	List<Sucursal> findByEmpresaId(int idEmpresa);
}
