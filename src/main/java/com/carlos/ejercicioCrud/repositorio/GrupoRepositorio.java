package com.carlos.ejercicioCrud.repositorio;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.carlos.ejercicioCrud.entidades.Grupo;

@Repository("GrupoRepositorio")
public interface GrupoRepositorio extends CrudRepository<Grupo, Serializable>{

	
}
