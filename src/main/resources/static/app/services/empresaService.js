APP.service('empresaService', function($q, factory){
    const self = this;
    const path = 'empresa';
    
    self.get = () =>{
        return $q((succes, error) => {
        	factory.get(path).then(
            		(resolve) => {
                 succes(resolve)
            }, 
            (reject) => {
                error(reject);
            })
        }) 
    }
    
    self.getById = (idUsuario) =>{
        return $q((succes, error) => {
        	factory.getById(path,idUsuario).then(
            		(resolve) => {
                 succes(resolve)
            }, 
            (reject) => {
                error(reject);
            })
        }) 
    }
    self.post = (task) =>{
        return $q((success, error) => {
        	factory.post(path, task).then(
            		(resolve) => {
                 success(resolve);
            }, 
            (reject) => {
                error(reject);
            })
        }) 
    }
    
    self.put = (task) =>{
        return $q((success, error) => {
        	factory.put(path, task).then(
                (resolve) => {
                    success(resolve)
                },
                (reject) => {
                    error(reject)
                })
        })
}
    
    self.delete = (task) =>{
        return $q((success, error) => {
        	factory.delete(path,task).then(
        			(resolve) => {
                 success(resolve);
            }, 
            (reject) => {
                error(reject);
            })
        }) 
    }

})