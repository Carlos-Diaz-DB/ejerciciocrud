const APP = angular.module("ejercicioCrud", ["ngRoute"]);

APP.config(function ($routeProvider) {
    $routeProvider
        .when("/empresa/:idGrupo", {
            templateUrl: "vistas/empresa.html",
            controller: "empresaCtrl"
        }).when("/sucursal/:idEmpresa", {
            templateUrl: "vistas/sucursal.html",
        controller: "sucursalCtrl"
        }).when("/grupo", {
            templateUrl: "vistas/grupo.html",
            controller: "grupoCtrl"
        })
         .otherwise({
          redirectTo: "/",
          templateUrl: "vistas/grupo.html",
        controller: "grupoCtrl"
         });
});