APP.controller("sucursalCtrl",function($scope,sucursalService,$location,$routeParams){


    $scope.dato2 = $routeParams.idEmpresa;

    //$scope.task = false;
    $scope.updateTaskBox = false;
    $scope.listaSucursal = {};
    $scope.empresaActualizar = {};


    $scope.verSeccion = function(){
        $scope.task ={};

        //$scope.updateTaskBox = true;
    }


//	   Mostrar notas
    $scope.obtenerSucursal = function () {
        sucursalService.getById($scope.dato2).then((data) => {
            $scope.listaSucursal = data;

        console.log($scope.listaSucursal);
    }, (reject) => {
            console.log("Ctrl: ", reject);
        });
    }

    $scope.submitForm = function(esValido){
        if(esValido){
            if($scope.sucursal.id){
                console.log("Enta a actualizar")
                $scope.actualizarSucursal();

            }else{
                //$scope.grupo.nombre = $scope.nombre;
                console.log("Enta a agregar")
                $scope.agregar();
            }
        }
    }



    $scope.agregar = function(){
        $scope.sucursal.status = '1';
        sucursalService.post($scope.sucursal).then((response) =>{
            console.log("Objeto de respuesta",response)

            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Grupo agregado',
                showConfirmButton: false,
                timer: 1500
            })
            $scope.obtenerSucursal();
        $scope.sucursal = null;
    },(reject) =>{
            console.log("Ctrl2:",reject);
        });

    }

    $scope.editar = function (task) {
        $scope.task = task;
        $scope.sucursal = task;
        console.log("Grupo",task)
//       $scope.verSeccion('Editar');
    }

//   Update task
    $scope.actualizarSucursal = function(){
        sucursalService.put($scope.sucursal).then((response) =>{
            console.log("Ctrl: ", response);
        $scope.obtenerSucursal();
        $scope.sucursal = null;
        $scope.task = null;

        Swal.fire({
            position: 'top-end',
            type: 'success',
            title: 'Se agrego el grupo',
            showConfirmButton: false,
            timer: 1500
        })
    },(reject) =>{
            console.log("Ctrl:",reject);
        });

    }



    $scope.eliminar = function(grupo){
        $scope.sucursal = grupo;
        $scope.sucursal.status = '0';

        Swal.fire({
            title: 'Estas seguro?',
            text: "Los cambios no se revertiran!",
            type: 'Advertencia',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Borrar'
        }).then((result) => {
            if (result.value) {
            sucursalService.put($scope.sucursal).then((response) =>{
                console.log("Ctrl: ", response);
            $scope.obtenerSucursal();
            $scope.sucursal = null;
            $scope.task = null;

            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Se agrego el grupo',
                showConfirmButton: false,
                timer: 1500
            })
        },(reject) =>{
                console.log("Ctrl:",reject);
            });

            Swal.fire(
                'Deleted!',
                'Gripo borrado correctamnbete.',
                'success'
            )
        }else{	$scope.obtenerSucursal();}
    })



    }

    $scope.cancel = function(){
        $scope.task = null;
    }

    const initController = function(){
        $scope.obtenerSucursal();
    }

    angular.element(document).ready(function (){
        initController();
    })

})
