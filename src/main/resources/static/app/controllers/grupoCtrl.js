APP.controller("grupoCtrl",function($scope,grupoService,$location){


	$scope.goTo2 = function(grupo){
		$scope.idGrupo = parseInt(grupo.id)
		console.log(grupo);
		$location.url('/empresa/' + grupo.id)
	}
	//$scope.task = false;
	$scope.updateTaskBox = false;
	$scope.listaGrupos = {};
	$scope.grupoActualizar = {};
	
	 
       $scope.verSeccion = function(){
        $scope.task ={};

        //$scope.updateTaskBox = true;
    }
		
	   
//	   Mostrar notas
$scope.obtenerGrupos = function () {
    grupoService.get().then((data) => {
        $scope.listaGrupos = data;

    }, (reject) => {
        console.log("Ctrl: ", reject);
    });
}
	
	$scope.submitForm = function(esValido){
		if(esValido){
			if($scope.grupo.id){
                console.log("Enta a actualizar")
                $scope.actualizarGrupo();
                
			}else{
                //$scope.grupo.nombre = $scope.nombre;
                console.log("Enta a agregar")
				$scope.agregar();
			}
		}
	}
	   
  

   $scope.agregar = function(){
    $scope.grupo.status = '1';
    grupoService.post($scope.grupo).then((response) =>{
		   console.log("Objeto de respuesta",response)
		   
		   Swal.fire({
			   position: 'top-end',
			   type: 'success',
			   title: 'Grupo agregado',
			   showConfirmButton: false,
			   timer: 1500
			 })
			$scope.obtenerGrupos();
		   $scope.grupo = null; 
	  },(reject) =>{
		  console.log("Ctrl2:",reject);
	  });
	   
   }
   
   $scope.editar = function (task) {
       $scope.task = task;
       $scope.grupo = task;
       console.log("Grupo",task)
//       $scope.verSeccion('Editar');
}
  
//   Update task
   $scope.actualizarGrupo = function(){
	   grupoService.put($scope.grupo).then((response) =>{
		   console.log("Ctrl: ", response);
		   $scope.obtenerGrupos();
           $scope.grupo = null; 
           $scope.task = null;
		   
		   Swal.fire({
			   position: 'top-end',
			   type: 'success',
			   title: 'Se agrego el grupo',
			   showConfirmButton: false,
			   timer: 1500
			 })
	  },(reject) =>{
		  console.log("Ctrl:",reject);
	  });
	   
   }
   

   
   $scope.borrar = function(grupo){
	   $scope.grupo = grupo;
	   $scope.grupo.status = '0';

	   Swal.fire({
		   title: 'Estas seguro?',
		   text: "Los cambios no se revertiran!",
		   type: 'Advertencia',
		   showCancelButton: true,
		   confirmButtonColor: '#3085d6',
		   cancelButtonColor: '#d33',
		   confirmButtonText: 'Borrar'
		 }).then((result) => {
		   if (result.value) {
		   grupoService.put($scope.grupo).then((response) =>{
			   console.log("Ctrl: ", response);
		   $scope.obtenerGrupos();
		   $scope.grupo = null;
		   $scope.task = null;

		   Swal.fire({
			   position: 'top-end',
			   type: 'success',
			   title: 'Se agrego el grupo',
			   showConfirmButton: false,
			   timer: 1500
		   })
	   },(reject) =>{
			   console.log("Ctrl:",reject);
		   });

		   Swal.fire(
		       'Deleted!',
		       'Gripo borrado correctamnbete.',
		       'success'
		     )
		   }else{	$scope.obtenerGrupos();}
		 })
	   
		   
	   
   }

   


  $scope.cancel = function(){
	  $scope.task = null;
  }
 
   
   const initController = function(){
   	$scope.obtenerGrupos();
   }
   
   angular.element(document).ready(function (){
   	initController();
})


   
  
   
 
})



