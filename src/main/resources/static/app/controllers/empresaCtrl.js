APP.controller("empresaCtrl",function($scope,empresaService,grupoService,$location,$routeParams){

	$scope.dato = $routeParams.idGrupo;
	$scope.updateTaskBox = false;
	$scope.listaempresas = {};
	$scope.empresaActualizar = {};
	
	 
       $scope.verSeccion = function(){
        $scope.task ={};
       }

    $scope.goTo3 = function(empresa){
        console.log(empresa);
        $location.url('/sucursal/' + empresa.id)
    }

    $scope.obtenerEmpresa = function () {
        empresaService.getById($scope.dato).then((data) => {
            $scope.listaempresas = data;
        }, (reject) => {
            console.log("Ctrl: ", reject);
        });
    }
	
	$scope.submitForm = function(esValido){
		if(esValido){
			if($scope.empresa.id){
                console.log("Actulaizar");
                $scope.actualizarEmpresa();
                
			}else{
				$scope.empresa.nombre = $scope.empresa;
				$scope.agregar();
			}
		}
	}
	   
  

   $scope.agregar = function(){

       grupoService.getById($scope.dato).then((response) => {
           $scope.grupo = response;
           console.log("Datos de grupo",response)
       })

    $scope.empresa.status = '1';
	$scope.empresa.grupo = $scope.grupo;
	console.log($scope.grupo);
    empresaService.post($scope.empresa).then((response) =>{
		   console.log("Objeto de respuesta",response)
		   
		   Swal.fire({
			   position: 'top-end',
			   type: 'success',
			   title: 'Grupo agregado',
			   showConfirmButton: false,
			   timer: 1500
			 })
			$scope.obtenerEmpresa();
		   $scope.empresa = null;
	  },(reject) =>{
		  console.log("Ctrl2:",reject);
	  });
	   
   }
   
   $scope.editar = function (task) {
       $scope.task = task;
       $scope.empresa = task;
//       $scope.verSeccion('Editar');
}
  
//   Update task
   $scope.actualizarEmpresa = function(){
    empresaService.put($scope.empresa).then((response) =>{
		   console.log("Ctrl: ", response);
		   $scope.obtenerEmpresa();
           $scope.empresa = null; 
           $scope.task = null;
		   
		   Swal.fire({
			   position: 'top-end',
			   type: 'success',
			   title: 'Se agrego el grupo',
			   showConfirmButton: false,
			   timer: 1500
			 })
	  },(reject) =>{
		  console.log("Ctrl:",reject);
	  });
	   
   }
   
// delete task section


	$scope.borrar = function(empresa){
		$scope.empresa = empresa;
		$scope.empresa.status = '0';

		Swal.fire({
			title: 'Estas seguro?',
			text: "Los cambios no se revertiran!",
			type: 'Advertencia',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Borrar'
		}).then((result) => {
			if (result.value) {
			empresaService.put($scope.empresa).then((response) =>{
				console.log("Ctrl: ", response);
			$scope.obtenerEmpresa();
			$scope.empresa = null;
			$scope.task = null;

			Swal.fire({
				position: 'top-end',
				type: 'success',
				title: 'Se agrego el grupo',
				showConfirmButton: false,
				timer: 1500
			})
		},(reject) =>{
				console.log("Ctrl:",reject);
			});

			Swal.fire(
				'Deleted!',
				'Gripo borrado correctamnbete.',
				'success'
			)
		}else{	$scope.obtenerEmpresa();}
	})
}

   


  $scope.cancel = function(){
	  $scope.task = null;
  }
 
   
   const initController = function(){
   	$scope.obtenerEmpresa();
   }
   
   angular.element(document).ready(function (){
   	initController();
})

})



